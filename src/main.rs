//    terra, a human-focused web directory
//    Copyright (C) 2020 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, version 3 of the License.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#![feature(proc_macro_hygiene, decl_macro, never_type)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;
#[macro_use] extern crate diesel;

mod api;
mod db;
mod token;
mod user;
mod schema;
mod site;
mod tags;

use diesel::prelude::*;
use dotenv::dotenv;
use std::collections::HashMap;
use rocket::http::Status;
use rocket::response::NamedFile;
use rocket_contrib::serve::StaticFiles;
use rocket_contrib::templates::Template;
use rocket_contrib::templates::tera::Context;
use site::Site;
use tags::Tags;
use urldecode::decode;

// Show the newest sites that were added and a list of tags
#[get("/")]
fn index(pooled_conn: db::DbConn) -> Result<Template,Status> {
    use db::sites::dsl::*;

    let connection = &*pooled_conn;
    // Get the first few sites from the db
    let results: Vec<Site> = sites.order(post_date.desc())
                                  // .limit(10)
                                  .load(connection)
                                  .map_err(|_| Status::InternalServerError)?;
    let mut context = Context::new();
    context.insert("sites", &results);
    context.insert("tags", &collect_tags(&results));
    Ok(Template::render("index", &context))
}

// Alphabetical sorting is MVP but more sorting might be useful
// and pagination might need to happen once the db grows big enough
#[get("/browse/<raw_search_tags..>")]
fn browse(pooled_conn: db::DbConn, raw_search_tags: Tags) -> Result<Template,Status> {
    use db::sites::dsl::*;
    // Actually get the Vec out of the Tags struct
    let &Tags(ref search_tags) = &raw_search_tags;

    let decoded_tags: Vec<String> = search_tags.iter().map(|y| decode(y.to_string())).collect();

    let connection = &*pooled_conn;
    // Get the sites that we should show to the user
    let results: Vec<Site> = sites.filter(tags.contains(&decoded_tags))
                                  .order(post_date.desc())
                                  .load(connection)
                                  .map_err(|_| Status::InternalServerError)?;

    // Then actually template them
    let mut context = Context::new();
    context.insert("sites", &results);
    context.insert("path", &decoded_tags);
    context.insert("tags", &collect_tags(&results).iter().filter(|(tag,_num)| !raw_search_tags.contains(*tag)).collect::<Vec<&(&str,u64)>>());
    let mut joined_paths = Vec::new();
    let mut acc = Vec::new();
    for tag in decoded_tags {
        acc.push(tag.clone());
        joined_paths.push((tag,acc.join("/")))
    }
    context.insert("joined_paths", &joined_paths);
    Ok(Template::render("browse", &context))
}

fn collect_tags(sites: &[Site]) -> Vec<(&str,u64)> {
    let mut tags: HashMap<&str,u64> = HashMap::new();

    for site in sites {
        for tag in site.tags.iter() {
            if let Some(count) = tags.get_mut(tag.as_str()) {
                *count += 1;
            } else {
                tags.insert(tag.as_str(),1);
            }
        }
    }
    let mut result: Vec<(&str,u64)> = tags.into_iter().collect();
    result.sort_unstable_by(
        |(tag_a,count_a), (tag_b,count_b)|
            // Order by count first
            if count_a != count_b {
                count_b.cmp(count_a)
            // But if both counts are equal then order alphabetically
            } else {
                tag_a.cmp(tag_b)
            });
    result
}

#[get("/favicon.ico")]
fn favicon() -> Result<NamedFile,Status> {
    NamedFile::open(concat!(env!("CARGO_MANIFEST_DIR"), "/static/favicon.ico"))
        .map_err(|_| Status::InternalServerError)
}

#[get("/about")]
fn about() -> Result<Template,Status> {
    let context = Context::new();
    Ok(Template::render("about", &context))
}

fn main() {
    // Load the dotenv file into the environment
    dotenv().ok();

    // Start rocket
    rocket::ignite()
        .mount("/static", StaticFiles::from(concat!(env!("CARGO_MANIFEST_DIR"), "/static")))
        .mount("/api", routes![api::auth, api::register, api::new_code, api::add_site, api::sites,
               api::remove_tag, api::add_tag, api::put_site, api::get_site, api::remove_site])
        .mount("/", routes![index,browse,favicon,about])
        .attach(Template::fairing())
        .attach(db::DbConn::fairing())
        .launch();
}
